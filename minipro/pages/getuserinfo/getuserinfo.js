// pages/getuserinfo/getuserinfo.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    issueId: '',
    status1: '',
    status2: '',
    status3: '',
    reasons: [{
        "title": "接受服务前初评",
        "value": "1"
      },
      {
        "title": "接受服务后的常规评估",
        "value": "2"
      },
      {
        "title": "状况发生变化后的即时评估",
        "value": "3"
      },
      {
        "title": "因评估结果有疑问进行的复评",
        "value": "4"
      }

    ],
    gender:[
      {
        "name":"男",
        "value":"1"
      },{
        "name":"女",
        "value":"2"
      }
    ],
    education:[
      {"name":"文盲","value":"1"},
      { "name": "小学", "value": "2" },
      { "name": "初中", "value": "3" },
      { "name": "高中/技校/中专", "value": "4" },
      { "name": "大学专科及以上", "value": "5" },
      { "name": "不详", "value": "6" }
    ],
    marriage:[
      { "name": "未婚", "value": "1" },
      { "name": "已婚", "value": "2" },
      { "name": "丧偶", "value": "3" },
      { "name": "离婚", "value": "4" },
      { "name": "未说明的婚姻状况", "value": "5" }
    ],
    reside:[
      { "name": "独居", "value": "1" },
      { "name": "与配偶/伴侣居住", "value": "2" },
      { "name": "与子女居住", "value": "3" },
      { "name": "与父母居住", "value": "4" },
      { "name": "与兄弟姐妹居住", "value": "5" },
      { "name": "与其他亲属居住", "value": "6" },
      { "name": "与非亲属关系的人居住", "value": "7" },
      { "name": "养老机构", "value": "8" }
    ],
    economics: [
      { "name": "退休金/养老金", "value": "1" },
      { "name": "子女补贴", "value": "2" },
      { "name": "亲友救助", "value": "3" },
      { "name": "其他补贴", "value": "4" }
    ],
    stupid:[
      { "name": "无", "value": "1" },
      { "name": "轻度", "value": "2" },
      { "name": "中度", "value": "3" },
      { "name": "重度", "value": "4" }
    ],
    mind:[
      { "name": "无", "value": "1" },
      { "name": "精神分裂症", "value": "2" },
      { "name": "双向情感障碍", "value": "3" },
      { "name": "偏执性精神障碍", "value": "4" },
      { "name": "分裂情感性障碍", "value": "5" },
      { "name": "癫痫所致精神障碍", "value": "6" },
      { "name": "精神发育迟滞性发精神障碍", "value": "7" }
    ],
    frequency:[
      { "name": "无", "value": "1" },
      { "name": "发生过1次", "value": "2" },
      { "name": "发生过2次", "value": "3" },
      { "name": "发生过3次及以上", "value": "4" }
    ]

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    if (options.issueId != undefined) {
      console.log(options)
      var that = this
      that.setData({
        issueId: options.issueId
      })
    }
  },
  //获取焦点
  focus1: function() {
    var that = this
    that.setData({
      status1: 0,
      status2: '',
      status3: '',
    })
    console.log(that.data.status1, that.data.status2, that.data.status3)
  },
  focus2: function() {
    var that = this
    that.setData({
      status1: '',
      status2: 0,
      status3: '',
    })
  },
  focus3: function() {
    var that = this
    that.setData({
      status1: '',
      status2: '',
      status3: 0,
    })
  },

  // 失去焦点

  blur1: function() {
    var that = this
    that.setData({
      status1: '',
      status2: '',
      status3: '',
    })
  },
  blur2: function() {
    var that = this
    that.setData({
      status1: '',
      status2: '',
      status3: '',
    })
  },
  blur3: function() {
    var that = this
    that.setData({
      status1: '',
      status2: '',
      status3: ''
    })
  },

  // 选择日期
  bindDateChange: function() {

  },
  // 选择评估原因
  radioChange: function(e) {
    console.log('选择评估原因：', e.detail.value)
  },
  // 选择性别
  genderChange:function(e){
    console.log('选择性别：', e.detail.value)
  },
  // 选择婚姻状况
  marriageChange: function (e) {
    console.log('选择婚姻状况', e.detail.value)
  },
  // 选择居住情况
  resideChange:function(e){
    console.log("选择居住情况",e.detail.value)
  },
  // 选择经济来源
  economicsChange: function (e) {
    console.log("选择经济来源", e.detail.value)
  },
  // 选择痴呆
  stupidChange: function (e) {
    console.log("选择痴呆", e.detail.value)
  },
  // 选择精神疾病
  mindChange: function (e) {
    console.log("选择精神疾病", e.detail.value)
  },
  // 跌倒
 tumbleChange: function (e) {
    console.log("跌倒", e.detail.value)
  },
  // 走失
  lostChange: function (e) {
    console.log("走失", e.detail.value)
  },
  // 噎食
  suffocateChange: function (e) {
    console.log("噎食", e.detail.value)
  },
  // 自杀
  suicideChange: function (e) {
    console.log("自杀", e.detail.value)
  },
  // goanswer
  goanswer:function(e){
    var that = this
    console.log(that.data.issueId)
    // issueId 问卷ID 上个页面传来的
    wx.navigateTo({
      url: '/pages/questionnaire/questionnaire?issueId=' + that.data.issueId,
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})