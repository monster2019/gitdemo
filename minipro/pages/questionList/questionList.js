// pages/questionList/questionList.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    issueData:[{
      "id":"111",
      "title":"校园安全问题调查",
      "describe":"这是对问卷的一大段话进行描述，表示这是一个医院的考试",
      "number":'20',
      "status":"1",
      "list":[]
      },
      {
        "id": "111",
        "title": "养老院调查小报告",
        "describe": "这是对问卷的一大段话进行描述，表示这是一个医院的考试",
        "number": '66',
        "status": "2",
        "list": []
      }
      ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },
// 点击跳转答题前信息填写
  goAnswer:function(e){
    // 获取题目id
    var that = this
    console.log(e.currentTarget.id)
    wx.navigateTo({
      url: "/pages/getuserinfo/getuserinfo?issueId=" + e.currentTarget.id,
    })

  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})